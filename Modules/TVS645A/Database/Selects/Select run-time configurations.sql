/*** Select run-time configurations.sql ***/

SELECT DISTINCT
  b.configuration_id,
  b.configuration_name,
  b.created_datetime
FROM @database.tvs645a_run_time a,
     @database.tvs645a_config_names b
WHERE a.data_run_id = @data_run_id
  AND a.configuration_id = b.configuration_id
