CREATE TABLE IF NOT EXISTS data_acquisition.tvs645a_designators
(
  channel_designator_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  unit_number TINYINT UNSIGNED NOT NULL,
  channel_number TINYINT UNSIGNED NOT NULL,
  channel_designator VARCHAR(120) NOT NULL,

  INDEX tvs645a_designators_pk (channel_designator_id),
  PRIMARY KEY (channel_designator_id),

  UNIQUE INDEX tvs645a_designators_ui (channel_designator)

);

