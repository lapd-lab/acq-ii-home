CREATE TABLE IF NOT EXISTS motion.receptacles
(
  receptacle_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,

  receptacle_number TINYINT NOT NULL,
  axis_count TINYINT UNSIGNED NOT NULL,

  INDEX receptacles_pk (receptacle_id),
  PRIMARY KEY (receptacle_id),
);


