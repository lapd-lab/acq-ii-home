CREATE TABLE IF NOT EXISTS motion.port_locations
(
  port_location_id TINYINT UNSIGNED NOT NULL,

  port_location_name VARCHAR(120) NOT NULL,

  INDEX port_locations_pk (port_location_id),
  PRIMARY KEY (port_location_id),
);


