SELECT
  a.physical_setup_id,
  a.saved_datetime,
  b.motion_device_id,
  c.motion_device_name,
  b.port_number,
  d.port_location_name,
  b.probe_id,
  e.probe_name,
  b.calibration_id,
  f.calibration_name,
  b.retracted_sx_cm,
  b.level_sy_cm
FROM motion.xy_physical_setup_ids a,
     motion.xy_physical_setups b,
     motion.motion_devices c,
     motion.port_locations d,
     motion.probes e,
     motion.xy_calibrations f
WHERE a.physical_setup_id = b.physical_setup_id
  AND b.motion_device_id = c.motion_device_id
  AND b.port_location_id = d.port_location_id
  AND b.probe_id = e.probe_id
  AND b.calibration_id = f.calibration_id
  AND a.physical_setup_id = @physical_setup_id