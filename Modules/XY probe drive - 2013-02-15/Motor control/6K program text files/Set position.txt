  ;Store motor position for this axis
  VARI3@axis = @motor_pos ;

  ;Set motor position for this axis
  PSET @arg_motor_pos  

  ;Store encoder position for this axis
  VARI4@axis = @encoder_pos  ;

  ;Set encoder position for this axis
  PESET @arg_encoder_pos
