INSERT INTO configuration.devices
  (
  device_name,
  device_type_id,
  device_description,
  device_bridge_vi_path,
  device_module_ip_address,
  device_module_vi_path,
  created_datetime
  )
SELECT "SIS 3301",
       a.device_type_id,
       "Struck Innovative Systeme 3301 8 channel ADC boards, 100 MHz.  Also provides access to SIS 3820 VME clock distributor.",
       "@ACQ_II_home\\Modules\\SIS 3301\\SIS 3301 bridge.vi",
       "@IP_address",
       "@ACQ_II_home\\Modules\\SIS 3301\\SIS 3301.vi",
       3168025745
FROM configuration.device_types a
WHERE a.device_type_name = "Data acquisition"

