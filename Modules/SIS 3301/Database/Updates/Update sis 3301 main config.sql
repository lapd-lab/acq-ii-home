/*** Update sis 3301 main config.sql ***/

UPDATE @database.sis_3301
SET clock_rate         = @clock_rate,
    trigger_mode       = @trigger_mode,
    stop_delay         = @stop_delay,
    software_start     = @software_start,
    shots_to_average   = @shots_to_average,
    samples_to_average = @samples_to_average,
    created_datetime   = @created_datetime
WHERE
  configuration_name = "@configuration_name"
