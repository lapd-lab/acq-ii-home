SELECT
  a.data_run_id,
  a.configuration_id,
  b.configuration_name,
  b.clock_rate,
  b.trigger_mode,
  b.stop_delay,
  b.software_start,
  b.shots_to_average,
  b.samples_to_average,
  b.created_datetime
FROM @database.sis_3301_assignments a, @database.sis_3301 b
WHERE a.configuration_id = b.configuration_id