@probe_name=Standard 0.250 inch B-dot probe,
@channel_name=Bx-dot;

@probe_name=Standard 0.250 inch B-dot probe,
@channel_name=By-dot;

@probe_name=Standard 0.250 inch B-dot probe,
@channel_name=Bz-dot;

@probe_name=Standard 0.375 inch B-dot probe,
@channel_name=Bx-dot;

@probe_name=Standard 0.375 inch B-dot probe,
@channel_name=By-dot;

@probe_name=Standard 0.375 inch B-dot probe,
@channel_name=Bz-dot;
