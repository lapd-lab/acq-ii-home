/*** Delete axis details.sql ***/

DELETE @database.axis_details
FROM @database.axis_details a, @database.axis_configurations b
WHERE a.configuration_id = b.configuration_id
  AND b.configuration_name = "@configuration_name"