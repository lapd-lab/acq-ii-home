/*** Select axis configs (all).sql ***/

SELECT
  a.configuration_name,
  a.axis_count,
  a.created_datetime
FROM @database.axis_configurations a