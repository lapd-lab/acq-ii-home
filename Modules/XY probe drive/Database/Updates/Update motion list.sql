/*** Update motion_list.sql ***/

UPDATE @database.motion_lists
SET
  motion_count=@motion_count,
  data_motion_count=@data_motion_count,
  created_datetime=@created_datetime
WHERE
  motion_list_name="@motion_list_name"
