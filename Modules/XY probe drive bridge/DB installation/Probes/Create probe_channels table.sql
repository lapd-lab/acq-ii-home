CREATE TABLE IF NOT EXISTS motion.probe_channels
(
  probe_id SMALLINT UNSIGNED NOT NULL,
  channel_name VARCHAR(120) NOT NULL,

  INDEX probe_channels_pk (probe_id, channel_name),
  PRIMARY KEY (probe_id, channel_name),

  INDEX probe_channels_fk1 (probe_id),
  FOREIGN KEY (probe_id) REFERENCES motion.probes(probe_id)
);


