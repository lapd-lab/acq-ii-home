/*** Update raw 2-axis config.sql ***/

UPDATE @database.axis_configurations
SET
  axis_count=@axis_count,
  created_datetime=@created_datetime
WHERE
  configuration_name="@configuration_name"
