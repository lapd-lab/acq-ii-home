/*** Select gpib run-time list.sql ***/

SELECT
  a.shot_number,
  b.gpib_channel,
  b.configuration_name,
  c.gpib_string
FROM @database.gpib_run_time a,
     @database.gpib_configurations b,
     @database.gpib_string_lists c
WHERE a.data_run_id = @data_run_id
  AND a.configuration_id = b.configuration_id
  AND a.configuration_id = c.configuration_id
  AND a.string_index = c.string_index
ORDER BY a.shot_number
