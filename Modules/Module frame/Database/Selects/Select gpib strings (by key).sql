/*** Select gpib strings (by key).sql ***/

SELECT
  a.string_index,
  a.gpib_string
FROM @database.gpib_string_lists a
WHERE a.configuration_id = @configuration_id
ORDER BY a.string_index
