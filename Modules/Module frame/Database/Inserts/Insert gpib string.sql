/*** Insert gpib string.sql ***/

INSERT INTO @database.gpib_string_lists
  (
  configuration_id,
  string_index,
  gpib_string
  )
SELECT
  a.configuration_id,
  @string_index,
  @gpib_string
FROM @database.gpib_configurations a
WHERE a.configuration_name = "@configuration_name"