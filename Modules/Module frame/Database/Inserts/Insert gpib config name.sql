/*** Insert gpib config name.sql ***/

INSERT INTO @database.gpib_configurations
  (
  configuration_name,
  gpib_channel,
  string_count,
  created_datetime
  )
VALUES
  (
  "@configuration_name",
  @gpib_channel,
  @string_count,
  @created_datetime
  )
