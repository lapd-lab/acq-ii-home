CREATE TABLE IF NOT EXISTS @database.tvs645a_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  configuration_id SMALLINT UNSIGNED NOT NULL,

  INDEX tvs645a_assignments_pk (data_run_id, configuration_id),
  PRIMARY KEY (data_run_id, configuration_id),

  INDEX tvs645a_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX tvs645a_assignments_fk2 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);
