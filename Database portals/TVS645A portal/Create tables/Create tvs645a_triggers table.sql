CREATE TABLE IF NOT EXISTS @database.tvs645a_triggers
(
  configuration_id SMALLINT UNSIGNED NOT NULL,
  unit_number TINYINT UNSIGNED NOT NULL,

  trigger_mode TINYINT NOT NULL,
  trigger_source TINYINT UNSIGNED NOT NULL,
  trigger_coupling TINYINT UNSIGNED NOT NULL,
  trigger_slope TINYINT NOT NULL,
  trigger_level DOUBLE NOT NULL,

  INDEX tvs645a_triggers_pk (configuration_id, unit_number),
  PRIMARY KEY (configuration_id, unit_number),

  INDEX tvs645a_triggers_fk1 (configuration_id),
  FOREIGN KEY (configuration_id) REFERENCES @database.tvs645a_config_names(configuration_id)
);


