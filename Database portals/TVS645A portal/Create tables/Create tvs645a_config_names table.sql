CREATE TABLE IF NOT EXISTS @database.tvs645a_config_names
(
  configuration_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  configuration_name VARCHAR(120) NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX tvs645a_config_names_pk (configuration_id),
  PRIMARY KEY (configuration_id),

  UNIQUE INDEX tvs645a_config_names_ui1 (configuration_name)
);
