INSERT INTO @database.tvs645a_verticals
  (
  configuration_id,
  unit_number,
  channel_number,
  LP_filtering,
  coupling,
  impedance,
  range_pp,
  offset,
  auto_gain
  )
VALUES
  (
  @configuration_id,
  @unit_number,
  @channel_number,
  @LP_filtering,
  @coupling,
  @impedance,
  @range_pp,
  @offset,
  @auto_gain
  )
