INSERT INTO @database.tvs645a_horizontals
  (
  configuration_id,
  unit_number,
  sample_rate,
  record_length,
  trigger_position,
  trigger_pos_units
  )
VALUES
  (
  @configuration_id,
  @unit_number,
  @sample_rate,
  @record_length,
  @trigger_position,
  @trigger_pos_units
  )
