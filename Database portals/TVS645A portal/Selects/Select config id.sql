SELECT
  a.configuration_id,
  a.configuration_name,
  a.created_datetime
FROM @database.tvs645a_config_names a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_name = "@configuration_name"
