SELECT
  a.configuration_id,
  a.unit_number,
  a.trigger_mode,
  a.trigger_source,
  a.trigger_coupling,
  a.trigger_slope,
  a.trigger_level
FROM @database.tvs645a_triggers a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
  AND a.configuration_id = @configuration_id
