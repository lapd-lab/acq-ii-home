INSERT INTO @database.tvs645a_assignments
  (data_run_id, configuration_id)
SELECT @data_run_id,
       a.configuration_id
FROM @database.tvs645a_config_names a
WHERE a.configuration_name = "@configuration_name"
