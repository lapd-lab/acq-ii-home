SELECT a.shot_number,
       a.channel_designator_id,
       a.byte_offset,
       a.byte_count,
       a.scale,
       a.offset,
       a.min,
       a.max,
       a.clipped

FROM @database.data_file_records a,
     @database.data_file_assignments b,
     configuration.devices c

WHERE a.data_file_id = b.data_file_id
  AND b.device_id = c.device_id
  AND b.data_run_id = @data_run_id
  AND c.device_name = "@device_name"
  AND a.shot_number = @shot_number
  AND a.channel_designator_id = @channel_designator_id

