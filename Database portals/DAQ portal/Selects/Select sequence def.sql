SELECT
  a.data_run_id,
  a.meta_sequence_id,
  a.line_number,
  a.line,
  a.shot_number,
  a.expanded_line_number,
  a.line_status,
  a.time_stamp

FROM @database.sequence_defs a

WHERE a.data_run_id = @data_run_id
  AND a.meta_sequence_id = @meta_sequence_id
ORDER BY a.line_number

