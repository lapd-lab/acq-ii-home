SELECT
  a.experiment_set_id,
  a.experiment_set_name,
  a.experiment_set_description,
  a.experiment_set_status,
  a.created_datetime
FROM configuration.experiment_sets a
WHERE 1 = 1 /* Kludge to allow "and clauses" */
