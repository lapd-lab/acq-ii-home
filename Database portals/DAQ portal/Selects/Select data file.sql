SELECT a.data_run_id,
       a.device_id,
       a.data_file_id,
       b.data_file_path,
       b.unique_name,
       b.total_byte_count

FROM @database.data_file_assignments a,
     @database.data_files b,
     configuration.devices c

WHERE a.data_file_id = b.data_file_id
  AND a.device_id = c.device_id
  AND a.data_run_id = @data_run_id
  AND c.device_name = "@device_name"

