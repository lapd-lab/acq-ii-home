INSERT INTO @database.experiments
  (
  experiment_name,
  experiment_description,
  experiment_set_id,
  investigator_id,
  current_status,
  status_datetime
  )
SELECT
  "@experiment_name",
  @experiment_description,
  a.experiment_set_id,
  b.investigator_id,
  "@current_status",
  @status_datetime
FROM configuration.experiment_sets a,
     configuration.investigators b
WHERE a.experiment_set_name = "@database"
  AND b.investigator_name = "@investigator_name"