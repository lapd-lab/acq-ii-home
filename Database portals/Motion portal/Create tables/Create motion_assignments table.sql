CREATE TABLE IF NOT EXISTS @database.motion_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  receptacle_id TINYINT UNSIGNED NOT NULL,

  motion_device_type_id TINYINT UNSIGNED NOT NULL,

  INDEX motion_assignments_pk (data_run_id, receptacle_id),
  PRIMARY KEY (data_run_id, receptacle_id),

  INDEX motion_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX motion_assignments_fk2 (receptacle_id),
  FOREIGN KEY (receptacle_id) REFERENCES motion.receptacles(receptacle_id),

  INDEX motion_assignments_fk3 (motion_device_type_id),
  FOREIGN KEY (motion_device_type_id) REFERENCES motion.motion_device_types(motion_device_type_id)
);


