CREATE TABLE IF NOT EXISTS @database.motion_list_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  motion_list_id SMALLINT UNSIGNED NOT NULL,

  INDEX motion_list_assignments_pk (data_run_id, motion_list_id),
  PRIMARY KEY (data_run_id, motion_list_id),

  INDEX motion_list_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX motion_list_assignments_fk2 (motion_list_id),
  FOREIGN KEY (motion_list_id) REFERENCES @database.motion_lists(motion_list_id)
);


