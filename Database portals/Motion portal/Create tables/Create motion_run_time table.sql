CREATE TABLE IF NOT EXISTS @database.motion_run_time
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  receptacle_id TINYINT UNSIGNED NOT NULL,
  shot_number INT UNSIGNED NOT NULL,

  motion_list_id SMALLINT UNSIGNED NOT NULL,
  motion_index INT UNSIGNED NOT NULL,
  x DOUBLE NOT NULL,
  y DOUBLE NOT NULL,

  INDEX motion_run_time_pk (data_run_id, receptacle_id, shot_number),
  PRIMARY KEY (data_run_id, receptacle_id, shot_number),

  INDEX motion_run_time_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX motion_run_time_fk2 (receptacle_id),
  FOREIGN KEY (receptacle_id) REFERENCES motion.receptacles(receptacle_id),

  INDEX motion_run_time_fk3 (motion_list_id),
  FOREIGN KEY (motion_list_id) REFERENCES @database.motion_lists(motion_list_id)
);


