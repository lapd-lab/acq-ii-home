CREATE TABLE IF NOT EXISTS @database.motion_lists
(
  motion_list_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  motion_list_name VARCHAR(120) NOT NULL,
  motion_count INT UNSIGNED NOT NULL,
  data_motion_count INT UNSIGNED NOT NULL,
  motion_device_type_id TINYINT UNSIGNED NOT NULL,
  created_datetime DOUBLE NOT NULL,

  INDEX motion_lists_pk (motion_list_id),
  PRIMARY KEY (motion_list_id),

  UNIQUE INDEX motion_lists_ui1 (motion_list_name),

  INDEX motion_lists_fk1 (motion_device_type_id),
  FOREIGN KEY (motion_device_type_id) REFERENCES motion.motion_device_types(motion_device_type_id)

);


