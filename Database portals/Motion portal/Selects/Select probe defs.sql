SELECT
  a.probe_id,
  a.probe_name,
  b.probe_type_name,
  a.xy_calibration_id,
  c.xy_calibration_name,
  a.channel_count,
  a.created_datetime
FROM motion.probes a,
     motion.probe_types b,
     motion.xy_calibrations c
WHERE 1 = 1  /* this allows optional clauses to always start with "AND" */
  AND a.status = "Active"
  AND a.probe_type_id = b.probe_type_id
  AND a.xy_calibration_id = c.xy_calibration_id
