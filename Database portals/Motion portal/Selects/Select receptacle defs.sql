SELECT
  a.receptacle_id,
  a.six_k_controller,
  a.axis
FROM motion.receptacle_defs a
ORDER BY a.six_k_controller, a.axis