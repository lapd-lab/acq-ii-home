SELECT
  a.data_run_id,
  a.motion_list_id,
  b.motion_list_name,
  b.motion_count,
  b.data_motion_count,
  b.created_datetime
FROM @database.motion_list_assignments a,
     @database.motion_lists b,
     motion.motion_device_types c
WHERE a.motion_list_id = b.motion_list_id
  AND b.motion_device_type_id = c.motion_device_type_id
