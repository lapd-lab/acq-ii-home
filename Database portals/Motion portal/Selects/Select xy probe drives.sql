SELECT
  a.data_run_id,
  b.receptacle_number,
  a.port_number,
  a.port_location_id,
  c.probe_name,
  d.xy_calibration_name,
  a.retracted_sx_cm,
  a.level_sy_cm
FROM @database.xy_probe_drives a,
     motion.receptacles b,
     motion.probes c,
     motion.xy_calibrations d
WHERE 1 = 1 /* so that clause can begin with AND */
  AND a.receptacle_id = b.receptacle_id
  AND a.probe_id = c.probe_id
  AND c.xy_calibration_id = d.xy_calibration_id
