SELECT
  a.probe_id,
  a.channel_name
FROM motion.probe_channels a,
     motion.probes b
WHERE a.probe_id = b.probe_id
