SELECT
  a.probe_type_id,
  a.probe_type_name
FROM motion.probe_types a
WHERE 1 = 1 /* Allows for and clause */
