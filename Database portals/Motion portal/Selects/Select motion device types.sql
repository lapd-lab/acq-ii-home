SELECT
  a.motion_device_type_id,
  a.motion_device_type_name
FROM motion.motion_device_types a
WHERE 1 = 1 /* Allows for and clause */
