SELECT
  a.motion_list_id,
  a.grid_center_x,
  a.grid_center_y,
  a.delta_x,
  a.delta_y,
  a.nx,
  a.ny
FROM @database.xy_motion_list_defs a
WHERE a.motion_list_id = @motion_list_id
