INSERT INTO @database.xy_probe_drives
  (data_run_id, receptacle_id, port_number, port_location_id, probe_id, retracted_sx_cm, level_sy_cm)
SELECT
  @data_run_id,
  a.receptacle_id,
  @port_number,
  @port_location_id,
  b.probe_id,
  @retracted_sx_cm,
  @level_sy_cm
FROM motion.receptacles a,
     motion.probes b
WHERE a.receptacle_number = @receptacle_number
  AND b.probe_name = "@probe_name"

