INSERT INTO motion.probe_channels
  (probe_id, channel_name)
SELECT a.probe_id,
       "@channel_name"
FROM motion.probes a
WHERE a.probe_name = "@probe_name"
