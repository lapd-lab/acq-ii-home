INSERT INTO @database.motion_list_assignments
  (data_run_id, motion_list_id)
SELECT
  @data_run_id,
  a.motion_list_id
FROM @database.motion_lists a
WHERE a.motion_list_name = "@motion_list_name"
