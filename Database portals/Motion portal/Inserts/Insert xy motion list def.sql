INSERT INTO @database.xy_motion_list_defs
  (motion_list_id, grid_center_x, grid_center_y, delta_x, delta_y, nx, ny)
SELECT
  a.motion_list_id,
  @grid_center_x,
  @grid_center_y,
  @delta_x,
  @delta_y,
  @nx,
  @ny
FROM @database.motion_lists a
WHERE motion_list_name = "@motion_list_name"


