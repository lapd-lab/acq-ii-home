INSERT INTO @database.motion_assignments
  (data_run_id, receptacle_id, motion_device_type_id)
SELECT
  @data_run_id,
  a.receptacle_id,
  b.motion_device_type_id
FROM motion.receptacles a,
     motion.motion_device_types b
WHERE a.receptacle_number = @receptacle_number
  AND b.motion_device_type_name = "@motion_device_type_name"

