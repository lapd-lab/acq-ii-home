INSERT INTO @database.xy_motions
  (motion_list_id, motion_number, x, y, take_data, data_motion_number)
SELECT
  a.motion_list_id,
  @motion_number,
  @x,
  @y,
  @take_data,
  @data_motion_number
FROM @database.motion_lists a
WHERE a.motion_list_name = "@motion_list_name"
