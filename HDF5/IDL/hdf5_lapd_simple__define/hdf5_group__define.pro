; NAME:
;   HDF5_group
;
; PURPOSE:
;   This object encapsulates operations pertaining to the HDF5 group.
;
; CALLING SEQUENCE:
;     HDF5_group = OBJ_NEW('HDF5_group')               ; initially creates the object
;     HDF5_group->Open, loc_id, name                   ; opens an HDF5 group attached under "loc_id"
;     HDF5_group->Create, loc_id, name                 ; creates an HDF5 group under "loc_id"
;
;     group_names = HDF5_group->Read_group_names()     ; returns the names of sub-groups attached under this group
;     sub_group = HDF5_group->Open_group(group_name)   ; opens a sub-group attached under this group
;     sub_group = HDF5_group->Create_group(group_name) ; creates a sub-group under this group
;                                                      ;   note: both the above functions return a group object
;
;     dataset_names = HDF5_group->Read_dataset_names() ; returns the names of datasets attached under this group
;     dataset = HDF5_group->Read_dataset(dataset_name) ; reads the specified dataset and returns it in an H5_PARSE
;                                                      ;   style structure
;     HDF5_group->Write_dataset, dataset_name, data    ; writes the specified data into a dataset under this group
;
;     attr = HDF5_group->Read_attribute(attr_name)     ; reads the specified attribute (only tested for strings)
;
;     status = HDF5_group->Get_error_status()          ; returns error information...
;     message = HDF5_group->Get_error_message()        ;
;     location = HDF5_group->Get_error_location()      ;
;
;     HDF5_group->Close                                ; closes the group (also done when object destroyed)
;     OBJ_DESTROY, HDF5_group                          ; destroys the object and closes the group, if open
;
; KEYWORD PARAMETERS:
;   HDF5_group::Init:
;   LOC_ID: The location id of the HDF5 file or group that contains the group to be opened.
;   NAME: The name of the group to be opened.
;   Example: create an HDF5_group object and open a specified group at the same time -- call is made from
;     within an HDF5_LaPD object:
;     Raw_group = OBJ_NEW('HDF5_group', LOC_ID=self.file_id, NAME='Raw data + config')
;
;   HDF5_group::Write_dataset:
;   CHUNK_DIMENSIONS: Must be of the same dimensionality as the data but with the same
;     or smaller extents.  For this method, where all the data are written at once,
;     the safest thing is to set chunk_dimensions to be the same as the data, which is
;     the default.  If chunk_dimensions is set the extents should divide evenly into
;     the data extents.
;   GZIP: Ranges from 0 (no compression) to 9 (max compression).
;   Example: write a dataset in this group with chunking and maximum compression:
;     test_data = FINDGEN(100, 100)
;     HDF5_group->Write_dataset, 'test_data', test_data, CHUNK_DIMENSIONS=[50, 50], GZIP=9
;
; ERROR HANDLING:
;   Execution will stop when an error is hit.  This is how the underlying HDF5
;   functions and procedures operate.
;
; MODIFICATION HISTORY:
;   Written by:  Jim Bamber  July, 2005.
;


;----------------------------------------------------------------------------
; HDF5_group::Init
;
; Purpose:
;  Initializes the HDF5_group object, optionally opening the specified group.
;
;  This function returns a 1 if initialization is successful, or 0 otherwise.
;
FUNCTION HDF5_group::Init, LOC_ID=loc_id, NAME=name

    self.error = OBJ_NEW('HDF5_error')

    IF ( (N_ELEMENTS(loc_id) EQ 1) AND (N_ELEMENTS(name) EQ 1) ) THEN $
         self->Open, loc_id, name

    RETURN, 1
END


;----------------------------------------------------------------------------
; HDF5_group::Cleanup
;
; Purpose:
;  Cleans up all memory associated with the HDF5_group object.
;
PRO HDF5_group::Cleanup

    self->Close
    OBJ_DESTROY, self.error

END


;----------------------------------------------------------------------------
; HDF5_group::Open
;
; Purpose:
;  Opens the specified group and activates this object.
;
PRO HDF5_group::Open, loc_id, name

    self.loc_id = loc_id
    self.name = name
    self.group_id = H5G_OPEN(loc_id, name)
    IF (self.group_id EQ 0) THEN $
         self.error->Handle_error, "Group: " + name + " could not be opened by H5G_OPEN()"

END


;----------------------------------------------------------------------------
; HDF5_group::Create
;
; Purpose:
;  Creates the specified group and activates this object.
;
PRO HDF5_group::Create, loc_id, name

    self.loc_id = loc_id
    self.name = name
    self.group_id = H5G_CREATE(loc_id, name)
    IF (self.group_id EQ 0) THEN $
         self.error->Handle_error, "Group: " + name + " could not be created by H5G_CREATE()"

END


;----------------------------------------------------------------------------
; HDF5_group::Close
;
; Purpose:
;  Closes the currently open group.
;
PRO HDF5_group::Close

    IF (self.group_id NE 0) THEN $
         H5G_CLOSE, self.group_id

    self.loc_id = 0
    self.group_id = 0
    self.name = ""

END


;----------------------------------------------------------------------------
; HDF5_group::Open_group
;
; Purpose:
;  Opens the specified sub-group assumed to be attached to the currently open
;  group and returns it as an object.  It is the responsibility of the caller
;  to destroy the returned HDF5_group object.
;
FUNCTION HDF5_group::Open_group, sub_group_name

    sub_group = OBJ_NEW('HDF5_group')
    sub_group->Open, self.group_id, sub_group_name

    RETURN, sub_group

END


;----------------------------------------------------------------------------
; HDF5_group::Create_group
;
; Purpose:
;  Creates the specified sub-group assumed to be attached to the currently open
;  group and returns it as an object.  It is the responsibility of the caller
;  to destroy the returned HDF5_group object.
;
FUNCTION HDF5_group::Create_group, sub_group_name

    sub_group = OBJ_NEW('HDF5_group')
    sub_group->Create, self.group_id, sub_group_name

    RETURN, sub_group

END


;----------------------------------------------------------------------------
; HDF5_group::Get_<property>
;
; Purpose:
;  This is a series of functions to return various internal properties.
;
FUNCTION HDF5_group::Get_error_status
    RETURN, self.error->Get_status()
END

FUNCTION HDF5_group::Get_error_message
    RETURN, self.error->Get_message()
END

FUNCTION HDF5_group::Get_error_call_stack
    RETURN, self.error->Get_call_stack()
END


;----------------------------------------------------------------------------
; HDF5_group::Read_attribute
;
; Purpose:
;  Returns the value corresponding to the specified name for an attribute attached
;  to the currently open group.
;
FUNCTION HDF5_group::Read_attribute, name
    attribute_id = H5A_OPEN_NAME(self.group_id, name)
    attribute = H5A_READ(attribute_id)
    H5A_CLOSE, attribute_id

    RETURN, attribute
END

;----------------------------------------------------------------------------
; HDF5_group::Write_attribute
;
; Purpose:
;  Writes data to the named attributed attached the currently open group. If
;  the attribute's dimensions or type are not the same as those of the new data,
;  then the old attribute is deleted and replaced with a compatible one.
;
;  TODO: Should create attribute if it doesn't exist
;
;  TODO: How to handle types (proposed):
;    dims same / type different: change type, unless /CONVERT_TYPE set, then 
;                                let H5 routines convert
;    dims diff / type same: new attribute, different dimensions
;    dims diff / type diff: new attribute with new dims, change type unless
;                           /CONVERT_TYPE set
;    need to better understand how hdf types are "equal"

pro HDF5_group::Write_attribute, name, data
    old = self->read_attribute(name)
    old_dim = size(old,/dimensions)
    dim = size(data,/dimensions)
    attribute_id = H5A_OPEN_NAME(self.group_id, name)
    if array_equal(dim, old_dim) then begin
        H5A_write, attribute_id, data
        H5A_CLOSE, attribute_id
    endif else begin
        datatype_id = h5t_idl_create(data)        
        h5a_close, attribute_id
        h5a_delete, self.group_id, name
        if array_equal(dim,0) then dim = 1
        dataspace_id = H5S_CREATE_SIMPLE(dim)
        attribute_id = h5a_create(self.group_id, name, datatype_id, dataspace_id)
        h5a_write, attribute_id, data
        h5a_close, attribute_id
        h5t_close, datatype_id
        h5s_close, dataspace_id
    endelse
END

;----------------------------------------------------------------------------
; HDF5_group::Read_group_names
;
; Purpose:
;  Returns the names of groups attached to the currently open group.
;
FUNCTION HDF5_group::Read_group_names

    RETURN, self->Read_object_names('GROUP')
END


;----------------------------------------------------------------------------
; HDF5_group::Read_dataset_names
;
; Purpose:
;  Returns the names of datasets attached to the currently open group.
;
FUNCTION HDF5_group::Read_dataset_names

    RETURN, self->Read_object_names('DATASET')
END


;----------------------------------------------------------------------------
; HDF5_group::Read_object_names
;
; Purpose:
;  Returns the names of objects attached to the currently open group of the
;  specified type.
;
FUNCTION HDF5_group::Read_object_names, type
    count = 0
    object_names = STRARR(100)

    member_count = H5G_GET_NMEMBERS(self.loc_id, self.name)
    FOR i = 0, member_count-1 DO BEGIN
         member_name = H5G_GET_MEMBER_NAME(self.loc_id, self.name, i)
         member_info = H5G_GET_OBJINFO(self.group_id, member_name)
         IF (member_info.TYPE EQ type) THEN BEGIN
              object_names[count] = member_name
              count = count + 1
         ENDIF
    ENDFOR

    IF (count EQ 0) THEN $
         object_names = STRARR(1) $
    ELSE $
         object_names = object_names[0:count-1]

    RETURN, object_names
END

;----------------------------------------------------------------------------
; HDF5_group::Read_dataset
;
; Purpose:
;  Returns a slice of dataset as an IDL array.  The slice selection parameters
;  are documented in the IDL help section for H5S_SELECT_HYPERSLAB, but briefly:
;    Each parameter is an array of length m, where m is the number of dimensions
;    in the dataset.
;    start: the starting index in each dimension.  Note that indices start at 0.
;    count: the number of elements to get in each dimension, or if the block
;           keyword is defined, the number of blocks to get in each dimension.
;    block: the number of elements that compose a block in each dimension.
;           defaults to 1 element.
;    stride: the number of elements (not blocks!) to move between selecting 
;            blocks. defaults to 1 element (don't skip any elements).  This 
;            must be greater than or equal to the block size.
;    As an example, suppose I have a dataset with 1000 elements and I want
;    elements 20-24, 120-124, 220-224, ..., 920-924.  Then I would use start=20,
;    count=10, block=5, stride=100.  The procedure would return an array of 
;    length 50.
;
FUNCTION HDF5_group::Read_dataset, dataset_name, $
                                   start, $
                                   count, $
                                   block=block, $
                                   stride=stride 
    dataset_id = H5D_OPEN(self.group_id, dataset_name)
    if n_elements(start) ne 0 then begin
        dataspace_id  = H5D_GET_SPACE(dataset_id)
        H5S_SELECT_HYPERSLAB, dataspace_id, start, count, block=block, $
                              stride=stride, /reset
        if n_elements(block) eq 0 then block = 1
        memoryspace_id = H5S_CREATE_SIMPLE(count*block)
        dataset = H5D_READ(dataset_id, file_space = dataspace_id, $
                           memory_space=memoryspace_id)
        H5S_CLOSE, dataspace_id
        H5S_CLOSE, memoryspace_id
    endif else begin
        dataset = H5D_READ(dataset_id)
    end
    H5D_CLOSE, dataset_id
    return, dataset
END

;----------------------------------------------------------------------------

function HDF5_group::get_dataset_dimensions, dataset_name
    dataset_id = h5d_open(self.group_id, dataset_name)
    dataspace_id = h5d_get_space(dataset_id)
    dims = h5s_get_simple_extent_dims(dataspace_id)
    h5s_close, dataspace_id
    h5d_close, dataset_id
    return, dims
end

;----------------------------------------------------------------------------
; HDF5_group::Write_dataset
;
; Purpose:
;  creates a dataset based on the data passed in, attached to the currently open
;  group, and with max_dimensions and chunk_dimensions as optionally specified.
;
;  TODO: delete dataset if it already exists
;

PRO HDF5_group::Write_dataset, dataset_name, $
                               data, $
                               CHUNK_DIMENSIONS=chunk_dimensions, $
                               GZIP=gzip, $
                               MEMBER_NAMES=member_names

     ; create a datatype
     if (n_elements(member_names) eq 0 ) then $
         datatype_id = H5T_IDL_CREATE(data) $
     else $
         datatype_id = H5T_IDL_CREATE(data, member_names=member_names)

     ; create a dataspace
     dimensions=size(data,/dimensions)
     dataspace_id = H5S_CREATE_SIMPLE(dimensions, MAX_DIMENSIONS=dimensions)

     ; create the dataset
     IF (N_ELEMENTS(chunk_dimensions) EQ 0) THEN $
          chunk_dimensions = dimensions
     IF (N_ELEMENTS(gzip) EQ 0) THEN $
          gzip = 0

     print, 'self.name: '+self.name
     print, 'self.group_id: ', self.group_id
     print, 'dataset_name: '+dataset_name
     print, 'datatype_id: ', datatype_id
     print, 'dataspace_id: ', dataspace_id
     print, 'chunk_dimensions: ', chunk_dimensions
     print, 'gzip: ', gzip
     print, 'dimensions: ', dimensions
     dataset_id = H5D_CREATE(self.group_id, dataset_name, datatype_id, $
         dataspace_id, CHUNK_DIMENSIONS=chunk_dimensions, GZIP=gzip)

     ; write the data to the dataset
     H5D_WRITE,dataset_id,data

     ; close identifiers
     H5D_CLOSE, dataset_id
     H5S_CLOSE, dataspace_id
     H5T_CLOSE, datatype_id

END


;----------------------------------------------------------------------------
; HDF5_group__define
;
; Purpose:
;  Defines the object structure for an HDF5_group object.
;
PRO HDF5_group__define
    struct = { HDF5_group, $
               loc_id: 0L, $
               group_id: 0L, $
               name: "", $
               error: OBJ_NEW('HDF5_error') $
             }
END







