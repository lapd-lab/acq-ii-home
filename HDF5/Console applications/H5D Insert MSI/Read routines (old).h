//========================================================================================
// Read routines.h : Utility routines for reading from the MSI string file.
//


bool read_bool( FILE* file );
int read_int( FILE* file );
float read_float( FILE* file );
void read_float_array( FILE* file, float *array, int count );
double read_double( FILE* file );

void read_bool_byte( FILE* file, char *mem_byte );
void read_int_bytes( FILE* file, char *mem_bytes );
void read_float_bytes( FILE* file, char *mem_bytes );

