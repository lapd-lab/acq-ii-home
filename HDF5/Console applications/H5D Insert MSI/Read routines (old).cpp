//========================================================================================
// Read routines.cpp : Utility routines for reading from the MSI string file.
//
// In this file:
//   bool read_bool( FILE* file )
//   int read_int( FILE* file )
//   float read_float( FILE* file )
//   void read_float_array( FILE* file, float *array, int count )
//   double read_double( FILE* file )
//


#include "stdafx.h"


//----------------------------------------------------------------------------------------
// Read bool
//
bool read_bool( FILE* file )
{
	bool value;
	fread( &value, 1, 1, file );

	return value;
}

	
//----------------------------------------------------------------------------------------
// Read int
//
int read_int( FILE* file )
{
	int *pointer;
	char value_chars[4], rev_value_chars[4];

	fread( value_chars, 4, 1, file );
	for ( int i=0; i<4; i++ ) rev_value_chars[i] = value_chars[3-i];
	pointer = (int *)rev_value_chars;
	int value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Read float
//
float read_float( FILE* file )
{
	float *pointer;
	char value_chars[4], rev_value_chars[4];

	fread( value_chars, 4, 1, file );
	for ( int i=0; i<4; i++ ) rev_value_chars[i] = value_chars[3-i];
	pointer = (float *)rev_value_chars;
	float value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Read float array
//
void read_float_array( FILE* file, float *array, int count )
{
	char *value_chars = (char *)malloc( count*4 );
	char *rev_value_chars = (char *)array;

	fread( value_chars, 4, count, file );
	for ( int j=0; j<count; j++ )
		for ( int i=0; i<4; i++ )
		{
			int offset = j*4;
			rev_value_chars[offset+i] = value_chars[offset+3-i];
		}

	free( value_chars );
}


//----------------------------------------------------------------------------------------
// Read double
//
double read_double( FILE* file )
{
	double *pointer;
	char value_chars[8], rev_value_chars[8];

	fread( value_chars, 8, 1, file );
	for ( int i=0; i<8; i++ ) rev_value_chars[i] = value_chars[7-i];
	pointer = (double *)rev_value_chars;
	double value = *pointer;

	return value;
}


//----------------------------------------------------------------------------------------
// Read bool byte
//
void read_bool_byte( FILE* file, char *mem_byte )
{
	fread( mem_byte, 1, 1, file );
}

	
//----------------------------------------------------------------------------------------
// Read int bytes
//
void read_int_bytes( FILE* file, char *mem_bytes )
{
	char file_bytes[4];

	fread( file_bytes, 4, 1, file );
	for ( int i=0; i<4; i++ ) mem_bytes[i] = file_bytes[3-i];
}


//----------------------------------------------------------------------------------------
// Read float bytes
//
void read_float_bytes( FILE* file, char *mem_bytes )
{
	char file_bytes[4];

	fread( file_bytes, 4, 1, file );
	for ( int i=0; i<4; i++ ) mem_bytes[i] = file_bytes[3-i];
}




