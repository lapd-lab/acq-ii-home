//========================================================================================
// H5D Insert Sequence.cpp : Defines the entry point for the console application.
//
// Usage:
//   H5D_Insert_Sequence.exe <HDF5 file> <index> <shot number> <data string length>
//
// <HDF5 file>: full path of the HDF5 file in which to insert a single shot of the data run sequence
// <index>: location within the dataset to insert the data
// <shot number>: usually not the same as index
// <data string length>: number of bytes of flattened data written to "C:\Temp\Sequence string.txt"
//
// No output file.
//
//
// In this file:
//
//	herr_t find_dataset(
//		hid_t file_id,
//		char *group_name,
//		char *dataset_name,
//		hid_t *group_id,
//		bool *found)
//
//	herr_t extend_create_sequence_dataset(
//		char* dataset,
//		bool found,
//		hid_t group_id,
//		int index,
//		hid_t* dataset_id,
//		hid_t* file_space_id,
//		hid_t* mem_space_id,
//		hid_t* mem_type_id )
//	
//   int _tmain(int argc, _TCHAR* argv[])


#include "stdafx.h"
#include "hdf5.h"


//----------------------------------------------------------------------------------------
// Find dataset
//
herr_t find_dataset(
	hid_t file_id,
	char *group_name,
	char *dataset_name,
	hid_t *group_id,
	bool *found)
{
	*found = false;
	fprintf( stdout, "Looking for group: %s...\n", group_name );
	fflush( stdout );
	*group_id = H5Gopen( file_id, group_name );
	if ( group_id < 0 ) { H5Eprint( stderr ); return -1; }

	hsize_t num_obj;
	herr_t err = H5Gget_num_objs( *group_id, &num_obj );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Loop through objects, looking for a match with the dataset name
	//
	fprintf( stdout, "Looking for dataset: %s...\n", dataset_name );
	fflush( stdout );
	char obj_name[1024];
	for ( hsize_t idx=0; idx<num_obj; idx++ )
	{
		ssize_t size = H5Gget_objname_by_idx( *group_id, idx, obj_name, 1024 );
		if ( size < 0 ) { H5Eprint( stderr ); return -1; }
		if ( strcmp(obj_name, dataset_name) == 0 ) { *found = true; break; }
	}
	if ( *found ) fprintf( stdout, "%s found\n", dataset_name );
	else fprintf( stdout, "%s not found\n", dataset_name );
	fflush( stdout );

	return 0;
}

	
//----------------------------------------------------------------------------------------
// Extend/Create sequence dataset
//
herr_t extend_create_sequence_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int line_count,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id )
{
	if ( found ) fprintf( stdout, "\nExtending dataset: " );
	else fprintf( stdout, "\nCreating dataset: " );
	fprintf( stdout, "%s...\n", dataset );
	fflush( stdout );
	herr_t err;


	// Open the dataset (if it was found)
	//
	if ( found )
	{
		*dataset_id = H5Dopen( group_id, dataset );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }
	}


	// File dataspace
	//
	int file_rank = 1;
	hsize_t file_dims[1] = { index+line_count };
	hsize_t file_maxdims[1] = { 4294967295 };

	if ( found )
	{
		*file_space_id = H5Dget_space( *dataset_id );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
		err = H5Sset_extent_simple( *file_space_id, file_rank, file_dims, file_maxdims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		*file_space_id = H5Screate_simple( file_rank, file_dims, file_maxdims );
		if ( *file_space_id < 0 ) { H5Eprint( stderr ); return -1; }
	}

	hsize_t start[1] = { index };
	hsize_t stride[1] = { 1 };
	hsize_t count[1] = { line_count };
	hsize_t block[1] = { 1 };
	err = H5Sselect_hyperslab( *file_space_id, H5S_SELECT_SET, start, stride, count, block );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory dataspace
	//
	int mem_rank = 1;
	hsize_t mem_dims[1] = { line_count };
	hsize_t mem_maxdims[1] = { line_count };
	*mem_space_id = H5Screate_simple( mem_rank, mem_dims, mem_maxdims );
	if ( *mem_space_id < 0 ) { H5Eprint( stderr ); return -1; }


	// Memory datatype
	//
	hid_t char_field_id = H5Tcopy( H5T_NATIVE_CHAR );
	hid_t short_field_id = H5Tcopy( H5T_NATIVE_SHORT );
	hid_t int_field_id = H5Tcopy( H5T_NATIVE_INT );
	hid_t float_field_id = H5Tcopy( H5T_NATIVE_FLOAT );
	hid_t double_field_id = H5Tcopy( H5T_NATIVE_DOUBLE );
	hid_t string120_field_id = H5Tcopy( H5T_C_S1 );  H5Tset_size( string120_field_id, 120 );
	hid_t string20_field_id = H5Tcopy( H5T_C_S1 );  H5Tset_size( string20_field_id, 20 );

	int byte_count = 156;
	*mem_type_id = H5Tcreate( H5T_COMPOUND, byte_count );
	if ( *mem_type_id < 0 ) { H5Eprint( stderr ); return -1; }
	err = H5Tinsert( *mem_type_id, "Line", 0, string120_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	err = H5Tinsert( *mem_type_id, "Shot number", 120, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	err = H5Tinsert( *mem_type_id, "Expanded line number", 124, int_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	err = H5Tinsert( *mem_type_id, "Line status", 128, string20_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	err = H5Tinsert( *mem_type_id, "Time stamp", 148, double_field_id );  if ( err < 0 ) { H5Eprint( stderr ); return -1; }

	H5Tclose( char_field_id );  H5Tclose( short_field_id );  H5Tclose( int_field_id );  H5Tclose( float_field_id );
	H5Tclose( double_field_id );  H5Tclose( string120_field_id );  H5Tclose( string20_field_id );


	if ( found )
	{
		// Extend the dataset
		//
		err = H5Dextend( *dataset_id, file_dims );
		if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	}
	else
	{
		// Dataset creation property list
		//
		hid_t plist_id = H5Pcreate( H5P_DATASET_CREATE );
		if ( plist_id < 0 ) { H5Eprint( stderr ); return -1; }

		hsize_t file_chunk[1] = { 1 };
		int compression = 4;
		H5Pset_chunk( plist_id, file_rank, file_chunk );
		H5Pset_deflate( plist_id, compression );


		// Everything is set up so now create the 2D dataset
		//
		*dataset_id = H5Dcreate( group_id, dataset, *mem_type_id, *file_space_id, plist_id );
		if ( *dataset_id < 0 ) { H5Eprint( stderr ); return -1; }

		H5Pclose( plist_id );
	}

	return 0;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	fprintf( stdout, "H5D_Insert_Sequence.exe\n" );
	fflush( stdout );
	// Expecting "H5D_Insert_Sequence.exe" plus 4 arguments
	//
	if ( argc != 5 )
	{
		fprintf( stderr, "argc = %d\n", argc );
		fflush( stderr );
		return -1;
	}


	// Read in the arguments
	//
	char HDF5_filename[1024];
	int index;
	int *shot_number = (int *)malloc( sizeof(int) );
	char *shot_number_bytes;
	int data_string_length;

	strcpy( HDF5_filename, argv[1] );
	index = atoi(argv[2]);
	*shot_number = atoi(argv[3]);
	shot_number_bytes = (char *) shot_number;
	data_string_length = atoi(argv[4]);

	fprintf( stdout, "HDF5_filename: %s\n", HDF5_filename );
	fprintf( stdout, "index: %d\n", index );
	fprintf( stdout, "*shot_number: %d\n", *shot_number );
	fprintf( stdout, "data_string_length: %d\n", data_string_length );
	fflush( stdout );


	// Open the HDF5 file
	//
	hid_t file_id = H5Fopen( HDF5_filename, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
	if ( file_id < 0 ) { H5Eprint( stderr ); return -1; }


	// ******************************************
	// *** Open and read the data string file ***
	//
	// Open the data string file and put the whole thing into a string.
	//
	FILE* data_string_file = fopen( "C:/Temp/Sequence string.txt", "rb" );
	char *data_string = (char *)malloc( data_string_length );
	// size_t fread( void *buffer, size_t size, size_t count, FILE *stream );
	size_t nread = fread( data_string, 1, data_string_length, data_string_file );
	fclose( data_string_file );

	fprintf( stdout, "nread: %d\n", nread );
	fflush( stdout );
	if ( nread != data_string_length )
	{
		int is_eof = feof( data_string_file );
		fprintf( stderr, "is_eof: %d\n", is_eof );
		fflush( stderr );
		return -1;
	}

	// Get the line count, then loop through the lines.
	//
	int str_index = 0;
	int line_count = sread_int( data_string, &str_index );
	fprintf( stdout, "line_count: %d\n", line_count );
	fflush( stdout );
	int byte_count = 156;
	char *sequence_summary = (char *)malloc( line_count*byte_count );
	for ( int i=0; i<line_count; i++ )
	{
		// Read sequence line structure (for each line)
		//
		sread_string( data_string, &(sequence_summary[0+i*byte_count]), &str_index );  // sequence line
		sread_int_bytes( data_string, &(sequence_summary[120+i*byte_count]), &str_index );  // shot_number
		sread_int_bytes( data_string, &(sequence_summary[124+i*byte_count]), &str_index );  // expanded_line_number
		sread_string( data_string, &(sequence_summary[128+i*byte_count]), &str_index );  // line_status
		sread_double_bytes( data_string, &(sequence_summary[148+i*byte_count]), &str_index );  // time_stamp
	}


	// ************************************
	// *** Process the sequence dataset ***
	//
	// This involves:
	//   1) looking for the dataset
	//   2) if found, extending it
	//   3) if not found, create it
	//   4) writing the lines into it
	//   5) closing the id's that were opened along the way
	//
	// First, though, declare the id's that we'll need
	//
	hid_t group_id;
	hid_t dataset_id;
	hid_t file_space_id;
	hid_t mem_space_id;
	hid_t mem_type_id;

	herr_t err;
	bool found;

	err = find_dataset( file_id, "/Raw data + config/Data run sequence", "Data run sequence", &group_id, &found );  if ( err < 0 ) return -1;
	err = extend_create_sequence_dataset( "Data run sequence",
		found, group_id, index, line_count, &dataset_id, &file_space_id, &mem_space_id, &mem_type_id );
	if ( err < 0 ) return -1;

	err = H5Dwrite( dataset_id, mem_type_id, mem_space_id, file_space_id, H5P_DEFAULT, sequence_summary );
	if ( err < 0 ) { H5Eprint( stderr ); return -1; }
	H5Tclose( mem_type_id ); H5Sclose( mem_space_id ); H5Sclose( file_space_id ); H5Dclose( dataset_id ); H5Gclose( group_id );


	// **********************************************
	// *** Free up HDF5 id's and allocated memory ***
	//
	H5Gclose( group_id );
	H5Fclose( file_id );

	return 0;
}


