//========================================================================================
// Read routines.h : Utility routines for reading from LabVIEW created strings and string
// files.
//


// String readers
//
bool sread_bool( char* string, int* index );
short sread_short( char* string, int* index );
int sread_int( char* string, int* index );
float sread_float( char* string, int index );
void sread_float_array( char* string, float *array, int count, int* index );
double sread_double( char* string, int* index );

void sread_bool_byte( char* string, char *mem_byte, int* index );
void sread_short_bytes( char* string, char *mem_bytes, int* index );
void sread_int_bytes( char* string, char *mem_bytes, int* index );
void sread_float_bytes( char* string, char *mem_bytes, int* index );
void sread_double_bytes( char* string, char *mem_bytes, int* index );
void sread_string( char* string, char *mem_bytes, int* index );


// File readers
//
bool read_bool( FILE* file );
short read_short( FILE* file );
void read_short_array( FILE* file, short *array, int count );
int read_int( FILE* file );
float read_float( FILE* file );
void read_float_array( FILE* file, float *array, int count );
double read_double( FILE* file );

void read_bool_byte( FILE* file, char *mem_byte );
void read_short_bytes( FILE* file, char *mem_bytes );
void read_int_bytes( FILE* file, char *mem_bytes );
void read_float_bytes( FILE* file, char *mem_bytes );
void read_double_bytes( FILE* file, char *mem_bytes );
void read_string( FILE* file, char *mem_bytes );

