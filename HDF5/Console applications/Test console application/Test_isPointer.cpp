//========================================================================================
// Test.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"
#include <iostream>       // std::cout


//----------------------------------------------------------------------------------------
// isPointer
//
template<typename T>
bool isPointer(T)
{
    return std::is_pointer<T>::value;
}


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
    // type_info::name example

    int i = 0;
    int *pi = NULL;

	std::cout << " isPointer(pi): " << isPointer(pi) << '\n';
	std::cout << " isPointer( i): " << isPointer( i) << '\n';
 
    return 0;
}


