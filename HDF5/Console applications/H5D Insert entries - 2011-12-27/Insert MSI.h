//========================================================================================
// Insert MSI.h
//

#include "hdf5.h"


herr_t extend_create_2D_MSI_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	int data_length,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t extend_create_summary_dataset(
	char* dataset,
	bool found,
	hid_t group_id,
	int index,
	hid_t* dataset_id,
	hid_t* file_space_id,
	hid_t* mem_space_id,
	hid_t* mem_type_id );

herr_t insert_MSI(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* MSI_index,
	int* error_index);


