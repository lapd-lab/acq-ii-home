//========================================================================================
// Insert configs.h
//

#include "hdf5.h"


herr_t insert_configs(
	_TCHAR* argv[],
	hid_t file_id,
	int shot_number,
	int* error_index );

herr_t H5A_create_write_compound(
	hid_t group_id,
	dataElement cluster,
	dataElement* reference_list,
	FILE* config_file );

herr_t H5A_create_write(
	hid_t group_id,
	dataElement element,
	dataElement* reference_list,
	FILE* config_file );

size_t getCharPtrSize( char *ptr );

herr_t check_descriptor_file(
	FILE* descriptor_file,
	hid_t file_id,
	char* efn,
	int* error_index,
	int shot_number );

herr_t process_config_file(
	FILE* config_file,
	char* device_name,
	hid_t file_id,
	char* efn,
	int* error_index,
	int shot_number );

