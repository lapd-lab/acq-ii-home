// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#include <iostream>
#include <tchar.h>
#include "stdio.h"
#include <string>
#include <signal.h>
#include <windows.h>

#include <H5public.h>
#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>


// TODO: reference additional headers your program requires here
//#include "Read routines.h"
//#include "Data structures.h"
#include "Error handling.h"
//#include "Insert devices.h"
//#include "Insert MSI.h"
//#include "Insert sequence lines.h"
//#include "Insert configs.h"
