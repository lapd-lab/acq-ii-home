//========================================================================================
// H5D Insert entries.cpp : Defines the entry point for the console application.
//
// Usage:
//   H5D_Insert_entries.exe <HDF5 file> <experiment set> <experiment> <data run> <shadow data root folder>
//     <device count> {<device name> list} <start time> <SIS crate root folder> <sleep count (ms)> 
//
// <HDF5 file>: full path of the HDF5 file in which to insert a single shot of device data
// <experiment set> <experiment> <data run>: 3 strings which locate the shot files
// <shadow data root folder>: this is where intermediate output files will be written
// <device count>: the number of devices in the data run
// {<device name> list}: the device names surrounded by double quotes, i.e. "SIS 3301" "6K Compumotor"
// <start time>: integer timestamp
// <SIS crate root folder>: kludge needed to include SIS crate averaging into the RT translator
// <sleep time (ms)>: recommended values: real-time mode -- 1000 ms, manual mode -- 10 ms
//
// Writes an error file if an error occurs.
// Writes an exit file when finished.
//
// This is based on H5D_Insert_devices.cpp.  It has been changed to loop until reading a
// termination flag.  It works on a single HDF5 file, opening and closing it on each shot.
// It also inserts all types of entries, namely:
//   -errors
//   -MSI
//   -sequence lines
//   -devices
//   -device configs
//


#include "stdafx.h"


//========================================================================================
//----------------------------------------------------------------------------------------
// Main
//
int _tmain(int argc, _TCHAR* argv[])
{
	char *version = "May 10 2012";


	// Expecting "H5D_Insert_entries.exe" plus 6+ndevices+3 arguments
	//
	if ( argc < 11 )  // must be at least one device
	{
		fprintf( stdout, "Expecting \"H5D_Insert_entries.exe\" plus 6+ndevices+3 arguments.  Error exit.\n" );
		return -1;
	}


	// Read in the first 5 arguments
	//
	char HDF5_filename[1024];
	char experiment_set[1024];
	char experiment[1024];
	char data_run[1024];
	char root_folder[1024];
	
	strcpy_s( HDF5_filename, 1024, argv[1] );
	strcpy_s( experiment_set, 1024, argv[2] );
	strcpy_s( experiment, 1024, argv[3] );
	strcpy_s( data_run, 1024, argv[4] );
	strcpy_s( root_folder, 1024, argv[5] );

	char experiment_folder[1024];
	char data_run_folder[1024];
	sprintf_s( experiment_folder, 1024, "%s/%s/%s", root_folder, experiment_set, experiment );
	sprintf_s( data_run_folder, 1024, "%s/%s", experiment_folder, data_run );

	char efn[1024];  // efn == error_out_filename, shortened to allow single line error-handling
	char exit_filename[1024];
	char log_filename[1024];
	sprintf_s( efn, 1024, "%s/Error.C++.dat", data_run_folder );
	sprintf_s( exit_filename, 1024, "%s/Exit.C++.dat", data_run_folder );
	sprintf_s( log_filename, 1024, "%s/Log.C++.dat", data_run_folder );


	// Read in the device count and check the argument count
	//
	int device_count;
	device_count = atoi(argv[6]);
	if ( argc != 10+device_count )
	{
		write_error( efn, 0, "Incorrect argument count.  Error exit." );
		FILE* exit_file;
		errno_t fopen_err = fopen_s( &exit_file, exit_filename, "w" );  if ( fopen_err ) return -1;
		fprintf( exit_file, "Incorrect argument count.  Error exit.\n" );
		fclose( exit_file );
		return -1;
	}


	// Print header and first 5 arguments
	//
	fprintf( stdout, "Version date: %s\n", version );
	fprintf( stdout, "H5D_Insert_entries.exe beginning...\n\n" );
	fprintf( stdout, "HDF5 filename: %s\n", HDF5_filename );
	fprintf( stdout, "Experiment set: %s\n", experiment_set );
	fprintf( stdout, "Experiment: %s\n", experiment );
	fprintf( stdout, "Data run: %s\n", data_run );
	fprintf( stdout, "Root folder: %s\n", root_folder );
	fprintf( stdout, "Device count: %d\n", device_count );

	FILE* log_file;
	errno_t fopen_err = fopen_s( &log_file, log_filename, "w" );  if ( fopen_err ) return -1;
	fprintf( log_file, "Version date: %s\n", version );
	fprintf( log_file, "H5D_Insert_entries.exe beginning...\n\n" );
	fprintf( log_file, "HDF5 filename: %s\n", HDF5_filename );
	fprintf( log_file, "Experiment set: %s\n", experiment_set );
	fprintf( log_file, "Experiment: %s\n", experiment );
	fprintf( log_file, "Data run: %s\n", data_run );
	fprintf( log_file, "Root folder: %s\n", root_folder );
	fprintf( log_file, "Device count: %d\n", device_count );


	// Print the device names
	//
	for ( int idevice=0; idevice<device_count; idevice++ )
	{
		char device_name[1024];
		strcpy_s( device_name, 1024, argv[7+idevice] );
		fprintf( stdout, "  %s\n", device_name );
		fprintf( log_file, "  %s\n", device_name );
	}


	// Read in and print the SIS crate data folder, sleep count, and timestamp
	//
	int timestamp;
	char SIS_crate_root_folder[1024];
	int sleep_time;

	timestamp = atoi(argv[7+device_count]);
	strcpy_s( SIS_crate_root_folder, 1024, argv[8+device_count] );
	sleep_time = atoi(argv[9+device_count]);

	fprintf( stdout, "timestamp: %d\n", timestamp );
	fprintf( stdout, "SIS crate data folder: %s\n", SIS_crate_root_folder );
	fprintf( stdout, "sleep count: %d\n", sleep_time );

	fprintf( log_file, "timestamp: %d\n", timestamp );
	fprintf( log_file, "SIS crate data folder: %s\n", SIS_crate_root_folder );
	fprintf( log_file, "sleep count: %d\n", sleep_time );

	fflush( stdout );
	fflush( log_file );


	// Setup for the loop
	//
	H5Eset_auto2( H5E_DEFAULT, NULL, NULL );  // turns off automatic printing of HDF5 errors
	hid_t file_id;

	char devices_filename[1024];
	char error_filename[1024];
	char MSI_filename[1024];
	char sequence_lines_filename[1024];
	char shot_signal_filename[1024];

	herr_t err = 0;
	int shot_number = 0;
	int sleep_count = 0;
	int max_sleep_count = 100000;
	int translation_complete = 0;

	sprintf_s( devices_filename, 1024, "%s/Devices/Devices.%06d.dat", data_run_folder, shot_number );
	sprintf_s( error_filename, 1024, "%s/Error/Error.%06d.dat", data_run_folder, shot_number );
	sprintf_s( MSI_filename, 1024, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number );
	sprintf_s( sequence_lines_filename, 1024, "%s/Sequence lines/Sequence lines.%06d.dat", data_run_folder, shot_number );
	sprintf_s( shot_signal_filename, 1024, "%s/Shot/Shot.%06d.dat", data_run_folder, shot_number );

	int devices_index = 0;
	int error_index = 0;
	int MSI_index = 0;
	int sequence_lines_index = 0;
	int file_timestamp = 0;
	char error_string[4096];  // dictate that error strings cannot be longer than 4096 characters
	bool error_occurred = false;


	// Loop indefinitely, processing "Shot.######.dat" files.  Keep track of time
	// since the last file appeared and stop when the time is too long, say
	// 100,000 seconds, i.e. a little more than 1 day.  Also, look within each
	// file for the "translation complete" flag to be true
	//
	FILE* shot_signal_file;
	while ( (sleep_count < max_sleep_count) && (translation_complete == 0) )
	{
		// Start by sleeping; recommended values: real-time mode -- 1000 ms, manual mode -- 10 ms
		//
		Sleep( sleep_time );
		sleep_count++;

		err = 0;
		file_timestamp = 0;


		// Look for the shot signal file
		//
		fopen_err = fopen_s( &shot_signal_file, shot_signal_filename, "rb" );
		fopen_err = 0;  // Not an error if file not found.
		if ( shot_signal_file != NULL ) file_timestamp = read_int( shot_signal_file );
		if ( file_timestamp == timestamp )
		{
			fprintf( stdout, "Shot %d: ", shot_number );
			fprintf( log_file, "Shot %d: ", shot_number );
			fflush( stdout );
			fflush( log_file );


			// Read translation complete flag
			translation_complete = read_int( shot_signal_file );
			fclose( shot_signal_file );


			// Open the HDF5 file
			//
			file_id = H5Fopen( HDF5_filename, H5F_ACC_RDWR, H5P_DEFAULT );  // hid_t H5Fopen(const char *name, unsigned flags, hid_t access_id )
			if ( file_id < 0 )
			{
				write_hdf5_error( efn, 0 );
				FILE* exit_file;
				fopen_err = fopen_s( &exit_file, exit_filename, "w" );  if ( fopen_err ) return -1;
				fprintf( exit_file, "Could not open HDF5 file.  Error exit.\n" );
				fprintf( log_file, "Could not open HDF5 file.  Error exit.\n" );
				fclose( exit_file );
				fclose( log_file );
				return -1;
			}


			// Do all the entries
			//
			// Devices
			if ( err == 0 )
			{
				FILE* devices_file;
				fopen_err = fopen_s( &devices_file, devices_filename, "rb" );
				fopen_err = 0;  // Not an error if file not found
				if ( devices_file != NULL )
				{
					fclose( devices_file );
					fprintf( stdout, "Devices " );    fflush( stdout );
					fprintf( log_file, "Devices " );  fflush( log_file );

					err = insert_devices( argv, file_id, shot_number, &devices_index, &error_index );
					if ( err == 0 )
						err = insert_configs( argv, file_id, shot_number, &error_index);

					if ( err != 0 )
					{
						error_occurred = true;
						strcpy_s( error_string, "Error inserting devices" );
						fprintf( stdout, "-- %s", error_string );    fflush( stdout );
						fprintf( log_file, "-- %s", error_string );  fflush( log_file );

						insert_error( file_id, &error_index, shot_number, error_string );
					}
				}
			}

			// MSI
			if ( err == 0 )
			{
				FILE* MSI_file;
				fopen_err = fopen_s( &MSI_file, MSI_filename, "rb" );
				fopen_err = 0;  // Not an error if file not found
				if ( MSI_file != NULL )
				{
					fclose( MSI_file );
					fprintf( stdout, "MSI " );    fflush( stdout );
					fprintf( log_file, "MSI " );  fflush( log_file );

					err = insert_MSI( argv, file_id, shot_number, &MSI_index, &error_index );

					if ( err != 0 )
					{
						error_occurred = true;
						strcpy_s( error_string, "Error inserting MSI" );
						fprintf( stdout, "-- %s", error_string );    fflush( stdout );
						fprintf( log_file, "-- %s", error_string );  fflush( log_file );

						insert_error( file_id, &error_index, shot_number, error_string );
					}
				}
			}

			// Sequence lines
			if ( err == 0 )
			{
				FILE* sequence_lines_file;
				fopen_err = fopen_s( &sequence_lines_file, sequence_lines_filename, "rb" );
				fopen_err = 0;  // Not an error if file not found
				if ( sequence_lines_file != NULL )
				{
					fclose( sequence_lines_file );
					fprintf( stdout, "Sequence lines " );    fflush( stdout );
					fprintf( log_file, "Sequence lines " );  fflush( log_file );
					
					err = insert_sequence_lines( argv, file_id, shot_number, &sequence_lines_index, &error_index );

					if ( err != 0 )
					{
						error_occurred = true;
						strcpy_s( error_string, "Error inserting sequence lines" );
						fprintf( stdout, "-- %s", error_string );    fflush( stdout );
						fprintf( log_file, "-- %s", error_string );  fflush( log_file );

						insert_error( file_id, &error_index, shot_number, error_string );
					}
				}
			}

			// Error
			if ( err == 0 )
			{
				FILE* error_file;
				fopen_err = fopen_s( &error_file, error_filename, "rb" );
				fopen_err = 0;  // Not an error if file not found
				if ( error_file != NULL )
				{
					fprintf( stdout, "Error " );    fflush( stdout );
					fprintf( log_file, "Error " );  fflush( log_file );
					read_string( error_file, error_string, 4096 );
					fclose( error_file );

					insert_error( file_id, &error_index, shot_number, error_string );
				}
			}


			H5Fclose( file_id );

			sleep_count = 0;
			shot_number++;
			sprintf_s( devices_filename, 1024, "%s/Devices/Devices.%06d.dat", data_run_folder, shot_number );
			sprintf_s( error_filename, 1024, "%s/Error/Error.%06d.dat", data_run_folder, shot_number );
			sprintf_s( MSI_filename, 1024, "%s/MSI/MSI.%06d.dat", data_run_folder, shot_number );
			sprintf_s( sequence_lines_filename, 1024, "%s/Sequence lines/Sequence lines.%06d.dat", data_run_folder, shot_number );
			sprintf_s( shot_signal_filename, 1024, "%s/Shot/Shot.%06d.dat", data_run_folder, shot_number );

			fprintf( stdout, "\n" );
			fprintf( log_file, "\n" );
			fflush( stdout );
		} // End if shot signal file was found and timestamps match
	} // End while loop

	FILE* exit_file;
	fopen_err = fopen_s( &exit_file, exit_filename, "w" );  if ( fopen_err ) return -1;
	if ( (err == 0) && (!error_occurred) )
	{
		fprintf( exit_file, "Normal exit.\n" );
		fprintf( stdout, "Normal exit.\n" );
		fprintf( log_file, "Normal exit.\n" );
	}
	else if ( (err == 0) && (error_occurred) )
	{
		fprintf( exit_file, "Normal exit (but at least 1 error occurred).\n" );
		fprintf( stdout, "Normal exit (but at least 1 error occurred).\n" );
		fprintf( log_file, "Normal exit (but at least 1 error occurred).\n" );
	}
	else
	{
		fprintf( exit_file, "Error exit.\n" );
		fprintf( stdout, "Error exit.\n" );
		fprintf( log_file, "Error exit.\n" );
	}
	fflush( stdout );
	fclose( exit_file );

	fprintf( log_file, "translation_complete: %d\n", translation_complete );
	fprintf( log_file, "sleep_count: %d\n", sleep_count );
	fprintf( log_file, "max_sleep_count: %d\n", max_sleep_count );
	fprintf( log_file, "timestamp: %d\n", timestamp );
	fprintf( log_file, "file_timestamp: %d\n", file_timestamp );
	fclose( log_file );

	return err;
}


