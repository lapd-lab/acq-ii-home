CREATE TABLE IF NOT EXISTS configuration.device_types
(
  device_type_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,

  device_type_name VARCHAR(120) NOT NULL,

  INDEX device_types_pk (device_type_id),
  PRIMARY KEY (device_type_id),
);


