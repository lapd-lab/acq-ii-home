CREATE TABLE IF NOT EXISTS configuration.experiment_sets
(
  experiment_set_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  experiment_set_name VARCHAR(120) NOT NULL, /* i.e. "Alfven wave experiments", one DB per experiment set */
  experiment_set_description TEXT NOT NULL,
  experiment_set_status ENUM("Active", "Inactive", "Archived") NOT NULL DEFAULT "Active",
  created_datetime DOUBLE NOT NULL,

  INDEX experiment_sets_pk (experiment_set_id),
  PRIMARY KEY (experiment_set_id),

  UNIQUE INDEX experiment_sets_ui1 (experiment_set_name),

);


