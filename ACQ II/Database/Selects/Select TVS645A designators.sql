/*** Select TVS645A designators.sql ***/

SELECT
  a.unit_number,
  a.channel_number
FROM data_acquisition.tvs645a_designators a
WHERE a.channel_designator_id = @channel_designator_id