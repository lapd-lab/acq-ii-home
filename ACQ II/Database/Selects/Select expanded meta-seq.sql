/*** Select expanded meta-seq.sql ***/

SELECT
  a.expanded_line,
  a.meta_line_number
FROM @database.expanded_meta_seq_defs a
WHERE a.meta_sequence_id = @meta_sequence_id
  AND a.data_run_id = @data_run_id
ORDER BY a.expanded_line_number
