/*** Select sis_3301 unique datasets.sql ***/

SELECT DISTINCT
  a.channel_designator_id,
  b.configuration_id,
  c.configuration_name
FROM @database.data_file_records a,
     @database.sis_3301_run_time b,
     @database.sis_3301 c
WHERE a.data_file_id = @data_file_id
  AND b.data_run_id = @data_run_id
  AND a.shot_number = b.shot_number
  AND b.configuration_id = c.configuration_id