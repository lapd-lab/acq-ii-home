/*** Select SIS 3301 dataset headers.sql ***/

SELECT
  a.shot_number,
  a.scale,
  a.offset,
  a.min,
  a.max,
  a.clipped
FROM @database.data_file_records a,
     @database.sis_3301_run_time b
WHERE a.data_file_id = @data_file_id
  AND a.channel_designator_id = @channel_designator_id
  AND a.shot_number = b.shot_number
  AND b.data_run_id = @data_run_id
  AND b.configuration_id = @configuration_id
ORDER BY a.shot_number