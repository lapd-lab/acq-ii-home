/*** Select tvs645a unique datasets.sql ***/

SELECT DISTINCT
  a.channel_designator_id,
  b.configuration_id,
  c.configuration_name
FROM @database.data_file_records a,
     @database.tvs645a_run_time b,
     @database.tvs645a_config_names c
WHERE a.data_file_id = @data_file_id
  AND b.data_run_id = @data_run_id
  AND a.shot_number = b.shot_number
  AND b.configuration_id = c.configuration_id