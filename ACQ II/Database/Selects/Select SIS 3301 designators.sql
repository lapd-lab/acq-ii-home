/*** Select SIS 3301 designators.sql ***/

SELECT
  a.board_number,
  a.channel_number
FROM data_acquisition.sis_3301_designators a
WHERE a.channel_designator_id = @channel_designator_id