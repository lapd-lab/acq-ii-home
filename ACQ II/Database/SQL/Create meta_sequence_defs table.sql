CREATE TABLE IF NOT EXISTS @database.meta_sequence_defs
(
  meta_sequence_id SMALLINT UNSIGNED NOT NULL,
  meta_line_number SMALLINT UNSIGNED NOT NULL,

  meta_line TEXT NOT NULL, 

  INDEX meta_sequence_defs_pk (meta_sequence_id, meta_line_number),
  PRIMARY KEY (meta_sequence_id, meta_line_number),

  INDEX meta_sequence_defs_fk1 (meta_sequence_id),
  FOREIGN KEY (meta_sequence_id) REFERENCES @database.meta_sequences(meta_sequence_id)

);


