CREATE TABLE IF NOT EXISTS @database.expanded_meta_seq_defs
(
  data_run_id SMALLINT UNSIGNED NOT NULL,
  meta_sequence_id SMALLINT UNSIGNED NOT NULL,
  expanded_line_number INT UNSIGNED NOT NULL,

  expanded_line TEXT NOT NULL, 
  meta_line_number SMALLINT UNSIGNED NOT NULL,

  INDEX expanded_meta_seq_defs_pk (data_run_id, meta_sequence_id, expanded_line_number),
  PRIMARY KEY (data_run_id, meta_sequence_id, expanded_line_number),

  INDEX sequence_defs_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX sequence_defs_fk2 (meta_sequence_id),
  FOREIGN KEY (meta_sequence_id) REFERENCES @database.meta_sequences(meta_sequence_id)

);


