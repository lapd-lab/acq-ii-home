CREATE TABLE IF NOT EXISTS @database.sequence_assignments
(
  data_run_id SMALLINT UNSIGNED NOT NULL,

  meta_sequence_id SMALLINT UNSIGNED NOT NULL,

  INDEX sequence_assignments_pk (data_run_id),
  PRIMARY KEY (data_run_id),

  INDEX sequence_assignments_fk1 (data_run_id),
  FOREIGN KEY (data_run_id) REFERENCES @database.data_runs(data_run_id),

  INDEX sequence_assignments_fk2 (meta_sequence_id),
  FOREIGN KEY (meta_sequence_id) REFERENCES @database.meta_sequences(meta_sequence_id)
);


