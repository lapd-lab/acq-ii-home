CREATE TABLE IF NOT EXISTS @database.data_files
(
  data_file_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,

  data_file_path TEXT NOT NULL,
  unique_name VARCHAR(255) NOT NULL, 
  total_byte_count DOUBLE NOT NULL, 

  INDEX data_files_pk (data_file_id),
  PRIMARY KEY (data_file_id),

  UNIQUE INDEX data_files_ui1 (unique_name)

);


