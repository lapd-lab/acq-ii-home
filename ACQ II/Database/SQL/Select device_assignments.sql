SELECT a.data_run_id,
       a.device_id,
       b.device_name,
       c.device_type_name

FROM @database.device_assignments a,
     configuration.devices b,
     configuration.device_types c

WHERE a.device_id = b.device_id
  AND b.device_type_id = c.device_type_id

