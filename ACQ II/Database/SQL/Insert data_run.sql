/*** Insert data_run.sql ***/

INSERT INTO @database.data_runs
  (
  experiment_id,
  data_run_name,
  data_run_description,
  experiment_set_id,
  investigator_id,
  current_status,
  status_datetime
  )
SELECT
  @experiment_id,
  "@data_run_name",
  @data_run_description,
  b.experiment_set_id,
  c.investigator_id,
  "@status_text",
  @status_datetime
FROM configuration.experiment_sets b,
     configuration.investigators c
WHERE b.experiment_set_name = "@database"
  AND c.investigator_name = "@investigator_name"
