Future changes to the typedefs will have to be versioned and the flatten and
unflatten VI's will have to go by these versions with defaults as appropriate.